package INF101.lab2;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class Fridge implements IFridge {

    int maxCapacity = 20;
    List<FridgeItem> Items = new ArrayList<>();
    List<FridgeItem> ExpiredFood = new ArrayList<>();
    //public void fridge(List<FridgeItem> Items) {
    //    this.Items = new ArrayList<>(Items);
    //}
    //public void expired(List<FridgeItem> ExpiredFood) {
    //    this.ExpiredFood = new ArrayList<>(ExpiredFood);
    //}

    @Override
    public int nItemsInFridge() {
        return Items.size();
    }

    @Override
    public int totalSize() {
        return maxCapacity;
    }

    @Override
    public boolean placeIn(FridgeItem item) {
        if(nItemsInFridge()<totalSize()) {
            Items.add(item);
            return true;
        } else{
            return false;
        }
    }

    @Override
    public void takeOut(FridgeItem item) {
        if(Items.contains(item)) {
            Items.remove(item);
        } else {
            throw new NoSuchElementException("fridge does not contain this " + item);
        }
    }

    @Override
    public void emptyFridge() {
        Items.clear();

    }

    @Override
    public List<FridgeItem> removeExpiredFood() {
        for (FridgeItem item : Items) {
            if(item.hasExpired()){
                ExpiredFood.add(item);
            }
        }
        for (FridgeItem item : ExpiredFood){
            Items.remove(item);
        }
        return ExpiredFood;
    }
}


